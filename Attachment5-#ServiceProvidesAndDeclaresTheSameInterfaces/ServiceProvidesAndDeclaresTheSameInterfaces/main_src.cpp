/*
 * Copyright (C) 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <hidl/LegacySupport.h>
#include <hidl/HidlTransportSupport.h>

#include <android-base/logging.h>
#include <android/binder_manager.h>
#include <android/binder_process.h>
#include <android/hardware/memtrack/1.0/IMemtrack.h>
#include <vendor/sprd/hardware/thermal/2.0/IExtThermal.h>

#include "include/vibrator-impl/Vibrator.h"
#include "lights.h"

using ::aidl::android::hardware::light::Lights;
using ::aidl::android::hardware::vibrator::Vibrator;
using android::hardware::memtrack::V1_0::IMemtrack;
using vendor::sprd::hardware::thermal::V2_0::IExtThermal;

using android::hardware::configureRpcThreadpool;
using android::hardware::joinRpcThreadpool;
using android::hardware::registerPassthroughServiceImplementation;

void *combined_hidl_thread(void *) {

    configureRpcThreadpool(2, true);

    android::status_t status_memtrack =
        registerPassthroughServiceImplementation<IMemtrack>();
    LOG_ALWAYS_FATAL_IF(
        status_memtrack != android::OK,
        "Error while registering light service: %d", status_memtrack);

    android::status_t status =
        registerPassthroughServiceImplementation<IExtThermal>();
    LOG_ALWAYS_FATAL_IF(
        status != android::OK,
        "Error while registering IExtThermal service: %d", status);

    joinRpcThreadpool();

    return NULL;
}

void combined_aidl_main_thread() {
    // increase thread count for each AIDL added
    ABinderProcess_setThreadPoolMaxThreadCount(1);

    std::shared_ptr<Lights> lights = ndk::SharedRefBase::make<Lights>();
    const std::string instance = std::string() + Lights::descriptor + "/default";
    binder_status_t status = AServiceManager_addService(lights->asBinder().get(), instance.c_str());
    CHECK(status == STATUS_OK);

    std::shared_ptr<Vibrator> vib = ndk::SharedRefBase::make<Vibrator>();

    const std::string vibra_instance = std::string() + Vibrator::descriptor + "/default";
    binder_status_t vibra_status = AServiceManager_addService(vib->asBinder().get(), vibra_instance.c_str());
    CHECK(vibra_status == STATUS_OK);

    ABinderProcess_joinThreadPool();

}

int main() {
    pthread_t t;
    pthread_create(&t, NULL, combined_hidl_thread, NULL);

    combined_aidl_main_thread();
    return EXIT_FAILURE;  // should not reach
}



